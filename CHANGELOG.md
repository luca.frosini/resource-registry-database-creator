This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for Resource Registry Database Creator

## [v2.0.0-SNAPSHOT]

- Switched JSON management to gcube-jackson [#19116]

## [v1.0.0] [r4.0.0] - 2016-07-27

- First Release