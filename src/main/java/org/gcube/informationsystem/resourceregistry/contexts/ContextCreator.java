package org.gcube.informationsystem.resourceregistry.contexts;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.gcube.common.scope.impl.ScopeBean;
import org.gcube.context.ContextElaborator;
import org.gcube.informationsystem.contexts.impl.entities.ContextImpl;
import org.gcube.informationsystem.contexts.reference.ContextState;
import org.gcube.informationsystem.contexts.reference.entities.Context;
import org.gcube.informationsystem.resourceregistry.api.contexts.ContextCache;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.contexts.ContextException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.contexts.ContextNotFoundException;
import org.gcube.informationsystem.resourceregistry.contexts.entities.ContextManagement;
import org.gcube.informationsystem.serialization.ElementMapper;
import org.gcube.resourcemanagement.support.shared.types.datamodel.D4SEnvironment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ContextCreator extends ContextElaborator {
	
	private static Logger logger = LoggerFactory.getLogger(ContextCreator.class);
	
	protected Map<String, UUID> contexts;
	protected ContextManagement contextManagement;
	
	public ContextCreator() {
		super();
		contexts = new HashMap<>();
		contextManagement = new ContextManagement();
	}
	
	protected Context create(Context context) throws ResourceRegistryException, IOException {
		ContextManagement contextManagement = new ContextManagement();
		contextManagement.setJson(ElementMapper.marshal(context));
		String contextString = contextManagement.create();
		logger.info("Created {}", contextString);
		Context c = ElementMapper.unmarshal(Context.class, contextString);
		c.setState(ContextState.ACTIVE.getState());
		contextManagement.setJson(ElementMapper.marshal(c));
		contextString = contextManagement.changeState();
		logger.info("Context activated {}", contextString);
		c = ElementMapper.unmarshal(Context.class, contextString);
		return c;
	}
	
	protected Context activate(Context context) throws ResourceRegistryException, IOException {
		context.setState(ContextState.ACTIVE.getState());
		ContextManagement contextManagement = new ContextManagement();
		contextManagement.setJson(ElementMapper.marshal(context));
		String contextString = contextManagement.update();
		logger.info("Created {}", contextString);
		Context c = ElementMapper.unmarshal(Context.class, contextString);
		return c;
	}
	
	protected UUID getContextUUID(ScopeBean scopeBean) throws ResourceRegistryException, IOException {
		if(scopeBean!=null) {
			try {
				UUID uuid = contexts.get(scopeBean.toString());
				if(uuid==null) {
					uuid = ContextCache.getInstance().getUUIDByFullName(scopeBean.toString());
					if(uuid!=null) {
						contexts.put(scopeBean.toString(), uuid);
					}
				}
				return uuid;
			}catch (ContextException e) {
				logger.info("{} does not exists", scopeBean);
			}
		}
		return null;
	}
	
	protected void elaborateContext(D4SEnvironment d4sEnvironment) throws Exception {
		UUID uuid = null;
		ScopeBean scopeBean = d4sEnvironment.getContext();
		
		UUID oldISResourceUUID = null;
		try {
			oldISResourceUUID = UUID.fromString(d4sEnvironment.getUuid());
		} catch (Exception e) {
			
		}
		try {
			uuid = getContextUUID(scopeBean);
		}catch (ContextNotFoundException e) {
			// The context has to be created
			logger.info("{} not found", scopeBean.toString());
		}
		if(uuid==null) {
			logger.info("Parent Context of {} is {}", scopeBean, scopeBean.enclosingScope());
			UUID parentUUID = getContextUUID(scopeBean.enclosingScope());
			Context context;
			if(oldISResourceUUID==null) {
				context = new ContextImpl(scopeBean.name());
			}else {
				context = new ContextImpl(scopeBean.name(), oldISResourceUUID);
			}
			if(parentUUID!=null) {
				context.setParent(parentUUID);
			}
			context = create(context);
			context = activate(context);
			contexts.put(scopeBean.toString(), context.getID());
		}else {
			logger.info("Context {} is already present with UUID {}", scopeBean, uuid.toString());
			if(oldISResourceUUID==null || uuid.compareTo(oldISResourceUUID)!=0) {
				String warn = "Context "  + scopeBean.toString() + " has in Resource Registry UUID " + uuid.toString() + " which does not match with UUID " + oldISResourceUUID + " of the resource in the OLD Information System";
				logger.warn(warn);
			}
		}
		
	}
	
	
	/*
	protected void elaborateContext(ScopeBean scopeBean) throws Exception {
		UUID uuid = null; 
		try {
			uuid = getContextUUID(scopeBean);
		}catch (ContextNotFoundException e) {
			// The context has to be created
			logger.info("{} not found", scopeBean.toString());
		}
		if(uuid==null) {
			logger.info("Parent Context of {} is {}", scopeBean, scopeBean.enclosingScope());
			UUID parentUUID = getContextUUID(scopeBean.enclosingScope()); 
			Context context = new ContextImpl(scopeBean.name());
			if(parentUUID!=null) {
				context.setParent(parentUUID);
			}
			context = create(context);
			contexts.put(scopeBean.toString(), context.getUUID());
		}else {
			logger.info("Context {} is already present with UUID {}", scopeBean, uuid.toString());
		}
	}
	*/
	
}

