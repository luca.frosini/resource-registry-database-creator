package org.gcube.informationsystem.resourceregistry.contexts;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.gcube.common.scope.impl.ScopeBean;
import org.gcube.context.ContextElaborator;
import org.gcube.informationsystem.resourceregistry.api.contexts.ContextCache;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.contexts.ContextException;
import org.gcube.resourcemanagement.support.shared.types.datamodel.D4SEnvironment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ContextTester extends ContextElaborator {
	
	private static Logger logger = LoggerFactory.getLogger(ContextTester.class);
	
	protected Map<String, UUID> contexts;
	
	public ContextTester() {
		super();
		contexts = new HashMap<>();
	}
	
	protected UUID getContextUUID(ScopeBean scopeBean) throws ResourceRegistryException, IOException {
		if(scopeBean!=null) {
			try {
				UUID uuid = contexts.get(scopeBean.toString());
				if(uuid==null) {
					uuid = ContextCache.getInstance().getUUIDByFullName(scopeBean.toString());
					if(uuid!=null) {
						contexts.put(scopeBean.toString(), uuid);
					}
				}
				return uuid;
			}catch (ContextException e) {
				logger.info("{} does not exists", scopeBean);
			}
		}
		return null;
	}
	
	protected void elaborateContext(D4SEnvironment d4sEnvironment) throws Exception {
		UUID uuid = null;
		ScopeBean scopeBean = d4sEnvironment.getContext();
		
		uuid = UUID.fromString(d4sEnvironment.getUuid());
		
		logger.info("Context {} in old Is has been identified by UUID {}", scopeBean, uuid.toString());
		
	}
	
}