package org.gcube.informationsystem.model.discovery;

import java.util.Collection;
import java.util.ServiceLoader;

import org.gcube.informationsystem.base.reference.Element;
import org.gcube.informationsystem.base.reference.entities.EntityElement;
import org.gcube.informationsystem.base.reference.properties.PropertyElement;
import org.gcube.informationsystem.base.reference.relations.RelationElement;
import org.gcube.informationsystem.discovery.Discovery;
import org.gcube.informationsystem.discovery.RegistrationProvider;
import org.gcube.informationsystem.serialization.ElementMapper;
import org.gcube.informationsystem.types.TypeMapper;
import org.gcube.informationsystem.types.reference.Type;
import org.gcube.informationsystem.types.reference.entities.EntityType;
import org.gcube.informationsystem.types.reference.properties.PropertyDefinition;
import org.gcube.informationsystem.types.reference.properties.PropertyType;
import org.gcube.informationsystem.types.reference.relations.RelationType;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class EntityDiscoveryTest {
	
	private static Logger logger = LoggerFactory.getLogger(EntityDiscoveryTest.class);
	
	private void addDiscoveredModel(Discovery<? extends Element> ismDiscovery) {
		ServiceLoader<? extends RegistrationProvider> regsitrationProviders = ServiceLoader
				.load(RegistrationProvider.class);
		for(RegistrationProvider registrationProvider : regsitrationProviders) {
			Collection<Package> packages = registrationProvider.getPackagesToRegister();
			for(Package p : packages) {
				ismDiscovery.addPackage(p);
			}
		}
	}
	
	@Test
	public void testPropertyDiscovery() throws Exception {
		Discovery<PropertyElement> propertyDiscovery = new Discovery<PropertyElement>(PropertyElement.class);
		addDiscoveredModel(propertyDiscovery);
		propertyDiscovery.discover();
		
		for(Class<? extends PropertyElement> propertyClass : propertyDiscovery.getDiscoveredElements()) {
			String json = TypeMapper.serializeType(propertyClass);
			logger.info("{}", json);
			Type typeDefinition = TypeMapper.deserializeTypeDefinition(json);
			logger.info("{}", ElementMapper.marshal(typeDefinition));
		}
	}
	
	@Test
	public void testEntityTypeDefinitionSerialization() throws Exception {
		logger.info("{}", TypeMapper.serializeType(EntityType.class));
	}
	
	@Test
	public void testDefinitionSerialization() throws Exception {
		logger.info("{}", TypeMapper.serializeType(Type.class));
		logger.info("{}", TypeMapper.serializeType(EntityType.class));
		logger.info("{}", TypeMapper.serializeType(RelationType.class));
		logger.info("{}", TypeMapper.serializeType(PropertyType.class));
		logger.info("{}", TypeMapper.serializeType(PropertyDefinition.class));
	}
	
	@Test
	public void testEntityDiscovery() throws Exception {
		Discovery<EntityElement> entityDiscovery = new Discovery<>(EntityElement.class);
		addDiscoveredModel(entityDiscovery);
		entityDiscovery.discover();
		
		for(Class<? extends EntityElement> entity : entityDiscovery.getDiscoveredElements()) {
			logger.info("{}", TypeMapper.serializeType(entity));
		}
	}
	
	@Test
	public void testRelationDiscovery() throws Exception {
		@SuppressWarnings("rawtypes")
		Discovery<RelationElement> relationDiscovery = new Discovery<>(RelationElement.class);
		addDiscoveredModel(relationDiscovery);
		relationDiscovery.discover();
		
		for(@SuppressWarnings("rawtypes") Class<? extends RelationElement> relation : relationDiscovery.getDiscoveredElements()) {
			logger.info("{}", TypeMapper.serializeType(relation));
		}
	}
	
	
	
}
