package org.gcube.informationsystem.resourceregistry;

import java.io.IOException;
import java.util.List;

import org.gcube.informationsystem.contexts.impl.entities.ContextImpl;
import org.gcube.informationsystem.contexts.reference.ContextState;
import org.gcube.informationsystem.contexts.reference.entities.Context;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.contexts.ContextCreator;
import org.gcube.informationsystem.resourceregistry.contexts.ContextTester;
import org.gcube.informationsystem.resourceregistry.contexts.ServerContextCache;
import org.gcube.informationsystem.resourceregistry.contexts.entities.ContextManagement;
import org.gcube.informationsystem.resourceregistry.dbinitialization.DatabaseEnvironment;
import org.gcube.informationsystem.serialization.ElementMapper;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class DataBaseCreator extends ContextTest {
	
	private static Logger logger = LoggerFactory.getLogger(DataBaseCreator.class);
	
	@Test
	public void createDatabase() throws Exception {
		ContextTest.setContextByName(GCUBE);
		logger.debug("Going to create DB");
		String db = DatabaseEnvironment.DB_URI;
		logger.debug("{} created", db);
	}
	
	@Ignore
	// @Test
	public void createAllContexts() throws Exception {
		ContextTest.setContextByName(GCUBE);
		ContextCreator contextCreator = new ContextCreator();
		contextCreator.all();
	}
	
	@Test
	public void checkAllContexts() throws Exception {
		ContextTest.setContextByName(GCUBE);
		ContextTester contextTester = new ContextTester();
		contextTester.all();
	}
	
	protected Context create(Context context) throws ResourceRegistryException, IOException {
		ContextManagement contextManagement = new ContextManagement();
		contextManagement.setJson(ElementMapper.marshal(context));
		String contextString = contextManagement.create();
		logger.info("Created {}", contextString);
		Context c = ElementMapper.unmarshal(Context.class, contextString);
		c.setState(ContextState.ACTIVE.getState());
		contextManagement.setJson(ElementMapper.marshal(c));
		contextString = contextManagement.changeState();
		logger.info("Context activated {}", contextString);
		c = ElementMapper.unmarshal(Context.class, contextString);
		return c;
	}
	
	@Ignore
	// @Test
	public void createDevContexts() throws Exception {
		Context gcube = new ContextImpl("gcube");
		gcube = create(gcube);
		
		Context devsec = new ContextImpl("devsec");
		devsec.setParent(gcube);
		devsec = create(devsec);
		
		Context devVRE = new ContextImpl("devVRE");
		devVRE.setParent(devsec);
		devVRE = create(devVRE);
		
		Context devNext = new ContextImpl("devNext");
		devNext.setParent(gcube);
		devNext = create(devNext);
		
		Context nextNext = new ContextImpl("NextNext");
		nextNext.setParent(devNext);
		nextNext = create(nextNext);
		
	}
	
	@Ignore
	// @Test
	public void activateAll() throws Exception {
		List<Context> contexts = ServerContextCache.getInstance().getFullInfoContexts();
		for(Context context : contexts) {
			context.setState(ContextState.ACTIVE.getState());
			ContextManagement contextManagement = new ContextManagement();
			contextManagement.setJson(ElementMapper.marshal(context));
			String contextString = contextManagement.changeState();
			logger.info("Context activated {}", contextString);
		}
	}
	
}
